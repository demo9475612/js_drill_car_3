function sortCarModelsAlphabetically(inventory) {
    if (!Array.isArray(inventory) || inventory.length === 0) {
        throw new Error('Invalid inventory');
    }

    const carModels= inventory.map(car => car.car_model) ;

    
    return carModels.sort();
    
}

module.exports = sortCarModelsAlphabetically;