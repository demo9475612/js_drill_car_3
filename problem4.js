function getAllCarYears(inventory) {
    if (!Array.isArray(inventory) || inventory.length === 0) {
        throw new Error('Invalid inventory');
    }
    return inventory.map(car => car.car_year);
}

module.exports = getAllCarYears;