function getBMWAndAudiCars(inventory) {
    if (!Array.isArray(inventory) || inventory.length === 0) {
        throw new Error('Invalid inventory');
    }
    return inventory.filter(car=>car.car_make === 'BMW' || car.car_make==='Audi');
}

module.exports = getBMWAndAudiCars;