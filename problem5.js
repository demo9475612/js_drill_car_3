function countOlderCars(inventory) {
    if (!Array.isArray(inventory) || inventory.length === 0) {
        throw new Error('Invalid inventory');
    }
    const carYears=inventory.map(car=>car.car_year);
    const olderCars=carYears.filter(year=>year < 2000);
    return olderCars.length;
}

module.exports = countOlderCars;