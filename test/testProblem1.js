const findCarById = require('../problem1');
const inventory = require('../inventory');

try{
    const car33 = findCarById(inventory,33);
    if(car33){
        console.log(`Car33 is a ${car33.car_year} ${car33.car_make} ${car33.car_model}`);
    }
    else{
        console.log('Car with Id 33 not found in the inventory.');
    }

 }catch(error){
    console.error(error.message);
 }
