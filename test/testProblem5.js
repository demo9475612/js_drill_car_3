const countOlderCars = require('../problem5');
const inventory = require('../inventory');

try {
    const olderCarsCount = countOlderCars(inventory);
    console.log(`Number of cars older than 2000: ${olderCarsCount}`);
} catch (error) {
    console.error(error.message);
}