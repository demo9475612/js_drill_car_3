const sortCarModelsAlphabetically = require('../problem3');
const inventory = require('../inventory');

try {
    const sortedModels = sortCarModelsAlphabetically(inventory);
    console.log("Sorted car Models : ", sortedModels);
} catch (error) {
    console.error(error.message);
}