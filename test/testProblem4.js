const getAllCarYears = require('../problem4');
const inventory = require('../inventory');

try {
    const carYears = getAllCarYears(inventory);
    console.log(carYears);
} catch (error) {
    console.error(error.message);
}